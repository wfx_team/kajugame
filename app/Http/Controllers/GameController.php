<?php

namespace App\Http\Controllers;

use App\Models\Game_detail;
use App\Models\Gameview;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class GameController extends Controller
{
    public function index()
    {
        self::trackViews();
//        dd(\Carbon\Carbon::parse('2012-9-5 23:26:11.223', 'Europe/Paris')->timezone->getName());
        $yesterday=\Carbon\Carbon::yesterday('Asia/Jayapura')->addHour(18)->toDateTimeString();
        $today=\Carbon\Carbon::yesterday('Asia/Jayapura')->addHour(42)->toDateTimeString();
//        dd(\Carbon\Carbon::yesterday()->addHour(42)->toDateTimeString());
        $leadboard = Game_detail::select('name', 'score')
//            ->whereBetween('updated_at', array($yesterday, $today))
            ->orderBy('score', 'DEC')
            ->limit(10)
            ->get();
        $played=Gameview::first();
        return view('game.index')
            ->with('played', $played)
            ->with('leadboard', $leadboard);
    }

    public function score(Request $request)
    {
        $request->session()->forget('score');
        $request->session()->put(['score'=> $request->score]);
    }

    public function user_details(Request $request)
    {
        $value = Session::get('score');
        $high = Game_detail::select(DB::raw('max(score) as max'))->first();
//        dd();

        if (!$value) {
            return redirect('game/play');
        }
        return view('game.user-details')
            ->with('score', $value)
            ->with('max', $high->max);
    }

    public function save_user_details(Request $request)
    {
        $existing = Game_detail::orwhere('email', $request->email)
            ->orWhere('phone', $request->phone)->first();
        if ($existing) {
            $existing->name = $request->name;
            $existing->score = $request->score;
            $existing->phone = $request->phone;
            $existing->facebook_id = $request->facebook_id;
            $result = $existing->save();
        } else {
            $game = new Game_detail($request->all());
            $result = $game->save();
            $smsMessage = "Thank you for participating Kaju Challenge\nSubha aluth auruddak wewa!";

        }
        if ($result) {
            return '1';
        } else {
            return '0';
        }
    }
    public function score_board()
    {
        $leadboard = Game_detail::select('name', 'score')
            ->orderBy('score', 'DEC')
            ->get();
//        dd($leadboard);
///
        return view('game.score-board')->with('leadboard', $leadboard);
//        dd('sfsdfd');
    }
    public function prizes()
    {

        return view('game.prizes');
    }
    public function trackViews()
    {
        $view=Gameview::first();
        $view->views =($view->views)+1;
        $view->save();

    }

}
