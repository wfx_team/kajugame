<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gameview extends Model
{
    public $timestamps = false;
    protected $fillable=['views'];
    //
}
