<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Game_detail extends Model
{
    protected $table ='game_details';
    protected $fillable =['name','phone','score','email','facebook_id'];
}
