<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GameController@index');


Route::get('game/play', 'GameController@index'); //view game page
Route::post('game/score/{id}', 'GameController@score'); //set score into session
Route::get('game/user-details/{id}', 'GameController@user_details'); //get user details
Route::post('game/save-user-details/{id}', 'GameController@save_user_details'); //save user details

Route::get('game/score-board', 'GameController@score_board'); //view game page
Route::get('game/prizes', 'GameController@prizes'); //view game page