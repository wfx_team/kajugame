/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function (e) {
    $('.search-panel .dropdown-menu').find('a').click(function (e) {
        e.preventDefault();
        var param = $(this).attr("href").replace("#", "");
        var concept = $(this).text();
        $('.search-panel span#search_concept').text(concept);
        $('.input-group #search_param').val(param);
    });
});

//START back to top js
$(function () {
    $(document).on('scroll', function () {
        if ($(window).scrollTop() > 100) {
            $('.scroll-top-wrapper').addClass('show');
        } else {
            $('.scroll-top-wrapper').removeClass('show');
        }
    });
    $('.scroll-top-wrapper').on('click', scrollToTop);
});
function scrollToTop() {
    verticalOffset = typeof (verticalOffset) != 'undefined' ? verticalOffset : 0;
    element = $('body');
    offset = element.offset();
    offsetTop = offset.top;
    $('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
}
//END back to top js

//START city search on popup window
$(function () {
    $(".search-result li").hide();
    $('#search').keyup(function () {
        var current_query_1 = $('#search').val();
        var current_query = toTitleCase(current_query_1);
        $('#all-srilanka-txt').html("");
        if (current_query !== "") {
            $(".search-result li").hide();
            $(".search-result li").each(function () {
                var current_keyword = $(this).text();
                if (current_keyword.indexOf(current_query) >= 0) {
                    $(this).show();
                    $(".popular-city").hide();
                    $('#all-srilanka-txt').html('Available Cities for "' + current_query_1 + '"');
                    $(".res-div").hide();

                } else {
                    $('#all-srilanka-txt').html('No Cities Available for "' + current_query_1 + '"');
                }
                ;
            });
        } else {
            $(".popular-city").show();
            $(".search-result li").hide();
            $('#all-srilanka-txt').html("");
            $(".res-div").show();
        };
    });
    $('.select-city-modal .close').click(function () {
        $('#search').val('');
        $(".popular-city").show();
        $(".search-result li").hide();
    });
});

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

//end city search on popup window

//switch login and register
$(function () {
    $('#login-form-link').click(function (e) {
        $(".loging-bottom").fadeOut(100);
        $("#login-form").delay(300).fadeIn(100);
        $(".loging-bottom").delay(1000).fadeIn(100);
        $("#register-form").fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        $('#log-reg-title').text('Login into Saleme.lk');
        e.preventDefault();
    });
    $('#register-form-link').click(function (e) {
        $(".loging-bottom").fadeOut(100);
        $("#register-form").delay(300).fadeIn(100);
        $(".loging-bottom").delay(1000).fadeIn(100);
        $("#login-form").fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
        $('#log-reg-title').text('Register with Saleme.lk');
        e.preventDefault();
    });

});
//switch login and register

