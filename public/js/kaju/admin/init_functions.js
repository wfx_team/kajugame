/**
 * Created by Niranjana on 10/21/2016.
 */

/*
 STATUS CHANGE
 <a href="javascript:;" class="statuschange" id="status_{{$result->id}}"
 uid="{{$result->id}}" status="{{$result->status}}" action="vmodel">
 {{$result->status}}
 </a>
 */
function statuschange() {
    $.ajaxSetup({
        headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')}
    });
    $('.statuschange').click(function () {
        var self = $(this);
        var id = self.attr("uid");
        var model = self.attr("action");
        var status = self.attr("status");
        var updatedStatus = (status == 'active') ? 'inactive' : 'active';
        $.ajax({
            url: '/default/statuschange',
            type: "post",
            data: 'id=' + id + '&status=' + updatedStatus + '&model=' + model,
            success: function (data) {
                if (data.response.message != '') {
                    self.html(updatedStatus);
                    self.attr('status', updatedStatus)
                    toastr["success"](data.response.message, "Success")
                } else {
                    toastr["danger"](data.response.error, "Danger")
                }
            }
        });
    });
}

/*
 RECORD DELETE
 <a href="javascript:;" class="delete" id="delete_{{$result->id}}" uid="{{$result->id}}" model="vmodel">
 <i class="fa fa-trash fa-1x" title="Delete"></i>
 </a>
 */
function deleterecord() {
    $('.delete').click(function () {
        alert();
        var self = $(this);
        var id = self.attr("uid");
        var model = self.attr("model");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            cancelButtonClass: 'btn-secondary waves-effect',
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajaxSetup({
                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')}
                });
                $.ajax({
                    url:'/default/deleterecord',
                    type: "post",
                    data: 'id=' + id + '&model=' + model,
                    success: function (res) {
                        if (res.response.message == 'success') {
                            // swal("Deleted!", "Your file has been deleted.", "success");
                            self.closest("tr").hide('slow');
                            // $('#row_' + id).hide('slow');
                        } else {
                            swal("Cancelled", "Your file is safe :)", "error");
                        }
                    }
                });
            }
        });
    });
}
