jQuery(document).ready(function ($) {
    $('#vehibrand').on('change', function () {
        var vehibrand = $(this).val();
        if (vehibrand) {
            $.ajax({
                type: 'POST',
                url: '/vehiclemodellist',
                data: 'vehibrand=' + vehibrand,
                success: function (data) {
                    var model = $('#vehimodel');
                    model.empty();
                    var options = '';
                    options += "<option value=''> Select Vehicel Model</option>";
                    $.each(data.vehimodellist, function (index, element) {
                        options += "<option value='" + element.modelid + "'>" + element.model_name + "</option>";
                    });
                    $("#vehimodel").html(options);
//  $('#vehimodel').selectpicker('refresh');
                }
            });

        }
    });

});