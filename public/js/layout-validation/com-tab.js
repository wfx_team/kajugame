/**
 * Created by Niranjana on 5/26/2017.
 */
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#btn_submit_signup').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
    $("#from-submit").validate({
        ignore: ":hidden:not(.selectpicker)",
        rules: {
            district_id: {
                required: true,
            },
            city_id: {
                required: true,
            },
            adtitle: {
                required: true,
            },
            description: {
                required: true,
            },
            price: {
                required: true,
                // notEqual: "0"
            },
            itemcondition: {
                required: true,
            },
            brand_id: {
                required: true,
            },
            model: {
                required: true,
            },
            itemtype: {
                required: true,
            }
        },
        messages: {
            district_id: {
                required: "Please Select District",
            },
            city_id: {
                required: "Please Select City",
            },
            adtitle: {
                required: "You must fill the Title for your Ad.",
            },
            description: {
                required: "You must fill the Description for your Ad.",
            },
            price: {
                required: "Enter price for your ad.",
            },
            itemcondition: {
                required: "You must select condition.",
            },
            brand_id: {
                required: "Please select brand.",
            },
            model: {
                required: "Please fill model.",
            },
            itemtype: {
                required: "Please select device type",
            }
        },
        submitHandler: function (form) {
            var myform = document.getElementById("from-submit");            var formData = new FormData(myform );
            submitHandlerForAjax(formData);
        }
    });
    jQuery.validator.addMethod("notEqual", function(value, element, param) {
        return this.optional(element) || value != param;
    }, "Price Can not be Zero (0)");
});