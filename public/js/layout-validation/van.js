/**
 * Created by Niranjana on 5/26/2017.
 */
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#btn_submit_signup').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
    $("#from-submit").validate({
        ignore: ":hidden:not(.selectpicker)",
        rules: {
            district_id: {
                required: true,
            },
            city_id: {
                required: true,
            },
            adtitle: {
                required: true,
            },
            description: {
                required: true,
            },
            price: {
                required: true,
                // notEqual: "0"
            },
            brand_id: {
                required: true,
            },
            model: {
                required: true,
            },
            registry_year: {
                required: true,
            },
            tranmission_id: {
                required: true,
            },
            enginesize: {
                required: true,
            },
            fuel_id: {
                required: true,
            },
            itemcondition: {
                required: true,
            },
            mileage: {
                required: true,
            },
        },
        messages: {
            district_id: {
                required: "Please Select District",
            },
            city_id: {
                required: "Please Select City",
            },
            adtitle: {
                required: "You must fill out the Title for your Ad.",
            },
            description: {
                required: "You must fill out description for your Ad..",
            },
            price: {
                required: "Enter price for your ad.",
            },
            brand_id: {
                required: "please fill out Brand",
            },
            model: {
                required: "please fill out model",
            },
            registry_year: {
                required: "please fill out model year",
            },
            tranmission_id: {
                required: "please select transmission type .",
            },
            enginesize: {
                required: "please fill engine capacity.",
            },
            mileage: {
                required: "please fill mileage.",
            },
            fuel_id: {
                required: "please select fuel type.",
            },
            itemcondition: {
                required: "please select condition.",
            },
        },
        submitHandler: function (form) {
            var myform = document.getElementById("from-submit");
            var formData = new FormData(myform);
            submitHandlerForAjax(formData);
        }
    });
    jQuery.validator.addMethod("notEqual", function (value, element, param) {
        return this.optional(element) || value != param;
    }, "Price Can not be Zero (0)");
});