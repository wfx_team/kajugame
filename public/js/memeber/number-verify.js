$(document).ready(function () {
    $('#verifynumber').hide();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $("#formNumberVerify").validate({
        rules: {
            number: {
                required: true,
                number: true,
                minlength: 9,
                maxlength: 10,
            },
        },
        messages: {
            number: {
                required: "Please enter valid number!",
            }
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: '/verifynumber',
                data: $("#formNumberVerify").serializeArray(),
                success: function (data) {
                    var info = data.response
                    console.log(info);
                    if (info.message == 'success') {
                        $('#verifynumber').show();
                        $('#message').html('OTP sent Successfully.');
                        $('#message').show();
                        $('#resend').val(info.number_for_resend);
                        $('#newnumber').val('');
                        $('#newnumber').hide();
                    }
                    //check if sms push a error code
                    if (info.message == 'error-code') {
                        $('#message').html('');
                        $('#message').html('SORRY! Something went wrong. Try again.');
                        $('#message').show();
                    }

                    if (info.message == 'verified') {
                        $('#message').html('Thanks you! Phone Number Verified.');
                        $('#message').show();
                        //reset the form
                        $("#formNumberVerify").trigger("reset");
                        $('#verifynumber').hide();
                        $('#newnumber').show();

                        //append the new verified number
                        $("#contact_nombers").append(
                                '<p class="m-0"><span class="label label-default">' + info.number + '</span> ' +
                                '<i class="ion-ios-checkmark-outline verify-icon" title="Verified"></i>' +
                                '<a  href="javascript:;" href="#" uid="' + info.number + '"  class="removephone" title="Delete"><i class="ion-ios-trash-outline del-icon"></i></a></p>');

                        //close the modal
                        setTimeout(function () {
                            $('#message').html('');
                            $('#modal').modal('toggle');
                        }, 1000);
                    }

                    if (info.error != '') {
                        $('#message').html(info.error);
                        $('#message').show();
                    }
                }
            })
        }
    });


});
$('#numberSubmit').on('click', function (e) {
    e.preventDefault();

    $('#formNumberVerify').submit();
});

$('#verify').on('click', function (e) {
    e.preventDefault();
    $('#formNumberVerify').submit();
});



