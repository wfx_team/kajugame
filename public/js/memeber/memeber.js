$(document).ready(function () {
    //  remove  mobile nomber 
    $('.removephone').click(function () {
        var self = $(this);
        var parent = self.parent().parent();
        if (confirm("Do you really want to delete?")) {
            $.ajax({
                type: 'POST',
                url: '/deletecontactno',
                data: 'uid=' + self.attr('uid'),
                dataType: 'json',
                success: function (msg) {
                    if (msg.success) {
                        $('#msg').html(msg.response);
                        $("#contact_"+ self.attr('uid')).remove();
                    } else {
                        $('#msg').html(msg.errors, 'errors');
                    }
                    return false;
                }
            });
        }
        return false;
    });

    //  remove email 
    $('.removeemail').click(function () {
        var self = $(this);
        var parent = self.parent().parent();
        if (confirm("Do you really want to delete?")) {
            $.ajax({
                type: 'POST',
                url: '/deleteemail',
                data: 'uid=' + self.attr('uid'),
                dataType: 'json',
                success: function (msg) {
                    if (msg.success) {
                        $('#msg').html(msg.response);
                        parent.fadeOut();
                    } else {
                        $('#msg').html(msg.errors, 'errors');
                    }
                    return false;
                }
            });
        }
        return false;
    });


    //  update member  details
    $("#update_form").validate({
        rules: {
            first_name: {required: true},
            gender: {required: true},
            district_id: {required: true},
            city_id: {required: true}
        },
        messages: {
            first_name: {required: "Please enter first name"},
            gender: {required: "Please Selsct Your Gender"},
            district_id: {required: "Please Select District"},
            city_id: {required: "Please Select City"}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: 'memberupdate',
                data: $("#update_form").serializeArray(),
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        location.reload();
                        $('.messager').removeClass('error-reg').addClass('success-reg');
                        $('.messager').html('<strong>' + data.response + '</strong>');

                    } else {
                        $('.messager').removeClass('success-reg').addClass('error-reg');
                        $('.messager').html('<strong>' + data.response + '</strong>');
                    }
                }
            })
        }
    });


    //  password change
    $("#updatepass_form").validate({
        rules: {
            old_password: {required: true},
            new_password: {required: true},
            confirm_password: {required: true, equalTo: "#new_password"}
        },
        messages: {
            old_password: {
                required: "Please enter old password"
            },
            new_password: {required: "Please new password"},
            confirm_password: {
                equalTo: "does not match password",
            },
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: 'updatepass',
//                url      : $(this).attr('action'),
                data: $("#updatepass_form").serializeArray(),
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        $('.messager').removeClass('error-reg').addClass('success-reg');
                        $('.messager').html('<strong>' + data.response + '</strong>');
                        $('#password_change').modal('toggle');
                        document.getElementById("updatepass_form").reset();
                        setTimeout(function () {
                            document.getElementById('close_btn_edit').click();
                            $('.messager').html('');
                        }, 2000);
                    } else {
                        $('.messager').removeClass('success-reg').addClass('error-reg');
                        $('.messager').html('<strong>' + data.response + '</strong>');
                    }
                }
            })
        }
    });
});

$('#memberupdate').on('click', function (e) {
    e.preventDefault();
    $('#update_form').submit();
});


