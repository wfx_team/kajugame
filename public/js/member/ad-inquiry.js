/**
 * Created by Niranjana on 5/26/2017.
 */
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#btn_submit_signup').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
    $("#inquiry-form").validate({
        rules: {
            name: "required",
            message: "required",
            email: {
                required: true,
                email: true
            },
            mobile: {
                required: true,
                number: true
            }
        }
    });

    $("#inquiry_post_ad").click(function (e) {
        e.preventDefault();
        if($("#inquiry-form").valid()){
            var formdata = $("#inquiry-form").serializeArray();
            $.ajax({
                type: "POST",
                url: '/member/sendinquirymail',
                data: formdata,
                success: function (res) {
                    if (res.errors) {
                        var li = '';
                        $.each(res.errors, function (key, value) {
                            li += '<li>' + value + '</li>'
                        });
                        $('#error-message').html(li);
                        $('#error-div').removeClass("hide");
                        $('html, body').animate({
                            scrollTop: $(".category-title").offset().top
                        }, 1000);
                        return false;
                    }
                    if (res.response.message == 'success') {
                        $('#modalinquiry').modal('hide')
                    }
                }
            });
        }
    });
});
