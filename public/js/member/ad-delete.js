/**
 * Created by Niranjana on 8/9/2017.
 */
var adid = '';
$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});
function notyConfirm(id) {
    noty({
        text: 'Do you want to continue?',
        layout: 'topRight',
        buttons: [{
            addClass: 'btn btn-success btn-clean', text: 'Delete', onClick: function ($noty) {
                adid = id;
                $('.deleresons').modal('show');

                // deleteRecord(id,table);
                $noty.close();
            }
        },
            {
                addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function ($noty) {
                $noty.close();
            }
            }
        ]
    })
}
// $(document).ready(function () {
$('#delete_btn1').click(function () {
    var reson_id = $('input[name="delete_reson_id"]:checked').val();
    var data = {'id': adid, 'delete_reson_id': reson_id};
    var classname = '.div-' + adid;
    $.ajax({
        type: "post",
        url: '/game/adreview-front/delete/' + adid,
        data: data,
        success: function (res) {
            $('.deleresons').modal('hide');
            $(classname).css('opacity', 0.5);
            noty({text: 'Ad deleted Successfully!', layout: 'topRight', type: 'success', timeout: 3000,});
            var delay = 5000;
            setTimeout(function () {
                window.location = '/my_ads';
            }, delay);
        }
    });
});
// });
// function deleteRecord(id,table){
// function deleteRecord(id,delete_reson_id){
//     // blocbtn(null);
//     alert(delete_reson_id);
//     // var data = {'id': id,'delete_reson_id':delete_reson_id};
//     // $.ajax({
//     //     type: "post",
//     //     url: '/adsmanage/adreview-front/delete/' + id,
//     //     data: data,
//     //     success: function (res) {
//     //         noty({text: 'Ad deleted Successfully!', layout: 'topRight', type: 'success',timeout: 3000,});
//     //         var delay = 5000;
//     //         // setTimeout(function () {
//     //         //     window.location = '/my_ads';
//     //         // }, delay);
//     //     }
//     // });
// }


// service delete
function notyServicedDelete(id) {
    noty({
        text: 'Do you want to Delete Service?',
        layout: 'topRight',
        buttons: [{
            addClass: 'btn btn-success btn-clean', text: 'Delete', onClick: function ($noty) {
                service_id = id;
                deleteService(service_id);

                // deleteRecord(id,table);
                $noty.close();
            }
        },
            {
                addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function ($noty) {
                $noty.close();
            }
            }
        ]
    })
}

function deleteService(service_id) {
    var data = {'id': service_id};
    $.ajax({
        type: "post",
        url: '/service/delete/' + service_id,
        data: data,
        success: function (res) {
            noty({text: 'Service deleted Successfully!', layout: 'topRight', type: 'success', timeout: 3000,});
            location.reload();
        }
    });
}

// gallery image delete
function notyImageDelete(id) {
    noty({
        text: 'Do you want to Delete this image from gallery?',
        layout: 'topRight',
        buttons: [{
            addClass: 'btn btn-success btn-clean', text: 'Delete', onClick: function ($noty) {
                image_id = id;
                deleteImage(image_id);
                $noty.close();
            }
        },
            {
                addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function ($noty) {
                $noty.close();
            }
            }
        ]
    })
}

function deleteImage(image_id) {
    var data = {'id': image_id};
    $.ajax({
        type: "post",
        url: '/image/delete/' + image_id,
        data: data,
        success: function (res) {
            noty({text: 'Image deleted Successfully!', layout: 'topRight', type: 'success', timeout: 3000,});
            location.reload();
        }
    });
}