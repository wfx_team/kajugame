/**
 * Created by Niranjana on 5/26/2017.
 */
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#btn_submit_signup').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
    $("#from-submit").validate({
        ignore: ":hidden:not(.selectpicker)",
        rules: {
            district_id: {
                required: true,
            },
            city_id: {
                required: true,
            },
            adtitle: {
                required: true,
            },
            description: {
                required: true,
            },
            price: {
                required: true,
                notEqual: "0"
                // min: 1
            },
            itemcondition: {
                required: true,
            },
            brand_id: {
                required: true,
            },
            model: {
                required: true,
            },
        },
        messages: {
            district_id: {
                required: "Please Select District",
            },
            city_id: {
                required: "Please Select City",
            },
            adtitle: {
                required: "You must fill out the Title for your Ad.",
            },
            description: {
                required: "You must fill out description for your Ad..",
            },
            price: {
                required: "Price cannot be 0.",
            },
            brand_id: {
                required: "You must fill out Brand field.",
            },
            model: {
                required: "You must fill out model field.",
            },
            itemcondition: {
                required: "please select condition.",
            }
        },
        submitHandler: function (form) {
            var formData = new FormData($("form")[0]);
            submitHandlerForAjax(formData);
        }
    });
    jQuery.validator.addMethod("notEqual", function(value, element, param) {
        return this.optional(element) || value != param;
    }, "Price Can not be Zero (0)");
});