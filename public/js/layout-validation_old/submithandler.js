/**
 * Created by Niranjana on 7/6/2017.
 */
function checkMainImage() {
    var at_type = $("#ad_type").val();
    if(at_type == 'sell' || at_type == 'rent' ){
        if (!$("input[name='featured']:checked").val()) {
            $("#dropzone_previews").addClass('featured-image-error');
            $('#error-message-image').css('color','#CC0000');
            $('#error-message-image').html('Please select Your Main Image!');
            $('html, body').animate({
                scrollTop: $("#image-upload").offset().top
            }, 500);
            return true;
        }
        return false;
    }
}
function submitHandlerForAjax(formData) {
    if(checkMainImage()){
        return;
    }
    $.ajax({
        type: "POST",
        url: '/vehicelpost',
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        beforeSend: function () {
            $.blockUI({message: '<h1>Just a moment...</h1>'});
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (data) {
            if (data.errors) {
                $.unblockUI();
                if('featuredImage' in data.errors){
                    if(data.errors.featuredImage != ''){
                        $("#dropzone_previews").addClass('featured-image-error');
                        $('#error-message-image').css('color','#CC0000');
                        $('#error-message-image').html(data.errors.featuredImage);
                        $('html, body').animate({
                            scrollTop: $("#image-upload").offset().top
                        }, 500);
                        return false;
                    }
                }

                var li = '';
                $.each(data.errors, function (key, value) {
                    li += '<li>' + value + '</li>'
                });
                $('#error-message').html(li);
                $('#error-div').removeClass("hide");
                $('html, body').animate({
                    scrollTop: $(".category-title").offset().top
                }, 1000);

                return false;
            }
            if (typeof data.url != 'undefined' && data.url != '') {
                $('#error-div').remove();
                var url = data.url;
                window.location = url + '?ref=' + data.referance;
            }
        }
    });
}



