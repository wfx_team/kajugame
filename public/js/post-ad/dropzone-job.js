/**
 * Created by Niranjana on 8/26/2017.
 */
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
});
var adId = $("#adId").val();
Dropzone.autoDiscover = false;
Dropzone.options.imageUpload = {
    maxFilesize: 5, // MB
    maxFiles: 1,
    parallelUploads: 1, //limits number of files processed to reduce stress on server
    addRemoveLinks: true,
    accept: function (file, done) {
        // TODO: Image upload validation
        done();
    },
    sending: function (file, xhr, formData) {
        // Pass token. You can use the same method to pass any other values as well such as a id to associate the image with for example.
        formData.append("_token", $('[name=_token]').val()); // Laravel expect the token post value to be named _token by default
    },
    init: function () {
        this.on("maxfilesexceeded", function (file) {
            this.removeAllFiles();
            this.addFile(file);
        });
        this.on("success", function (file, response) {
            // On successful upload do whatever :-)
            myDropzone.removeFile(file);
        });
    },
    success: function (file, response) {
        if (response.errors != '') {
            $('#error-message').html(response.errors);
            $('#error-div').removeClass("hide");
            $('html, body').animate({
                scrollTop: $(".category-title").offset().top
            }, 1000);
            return false;
        }
        if (response.errors == '') {
            var html = '';
            var last_id = response.lastId.id;
            var checked = response.lastId.checked;
html += '<div class="col-xs-6 col-md-3  col-sm-3 dz-prev-img-divs" id="res_' + last_id + '">' +
                '<div class="thumbnail">' +
                '<a href="#" class=""> ' +
                '<img src="' + response.image + '"> ' +
                '</a>' +
                '<span class="display-block">' +
                '<input type="radio" id="featured_' + last_id + '" name="featured" value="' + last_id + '" ' + checked + ' onclick="setFeatured(' + last_id + ')"> ' +
                '<label for="featured_' + last_id +'"> Main Image</label>' +
                '</span>' +
                '<a href="javascript:;" onclick="tempDelete(' + last_id + ')" class="delete-link"><i class="ion-trash-a"></i> Delete</a>' +
                '</div>' +
                '</div>';            
            $("#dropzone_previews").append(html);
        }
    }
};

var myDropzone = new Dropzone("#image-upload", {
    url: '/salemeupload/storejob',
    params: {
        adId: adId
    }
});

function setFeatured(value) {
    $.ajax({
        type: "POST",
        url: '/salemeupload/featured/' + value,
        data: {id: value},
        beforeSend: function () {
//                $.blockUI({message: '<h1>Just a moment...</h1>'});
        },
        complete: function () {
//                $.unblockUI();
        },
        success: function (data) {
            if(data.message == 'success'){
                $("#dropzone_previews").removeClass('featured-image-error');
                $('#error-message-image').html('');
            }
        }
    });
}

function tempDelete(value) {
    $.ajax({
        type: "POST",
        url: '/salemeupload/delete/' + value,
        data: {id: value},
        success: function (res) {
            if (res.message == 'success') {
                $("#res_" + value).toggle();
            }
        }
    });
}

function deleteFromAdImages(value) {
    $.ajax({
        type: "POST",
        url: '/uploadimages/delete/' + value,
        data: {id: value},
        success: function (res) {
            if (res.message == 'success') {
                $("#res_" + res.id).toggle();
            }
        }
    });
}
