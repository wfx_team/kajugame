jQuery(document).ready(function ($) {
    $('#district_id').on('change', function () {
        var districtid = $(this).val();
        $.ajax({
            type: 'POST',
            url: '/citylist',
            data: 'districtid=' + districtid,
            success: function (data) {
                var model = $('#city_id');
                model.empty();
                var options = '';
                options += "<option value=''> Select Area</option>";
                $.each(data.citylist, function (index, element) {

                    options += "<option value='" + element.id + "'>" + element.city_name + "</option>";
                });
                $("#city_id").html(options);
                $('#city_id').selectpicker('refresh');
            }
        });
    });

});