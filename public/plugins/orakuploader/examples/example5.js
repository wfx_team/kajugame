$(document).ready(function(){
	$('#images5ex').orakuploader({
		orakuploader_path : 'orakuploader/',
		
		orakuploader_main_path : 'files',
		orakuploader_thumbnail_path : 'files/tn',
		
		orakuploader_add_image : 'orakuploader/images/add.png',
		orakuploader_add_label : 'Browser for images',
		
		orakuploader_resize_to : 600,
		orakuploader_thumbnail_size : 150,
		
		orakuploader_maximum_uploads : 3,
		orakuploader_hide_on_exceed : true,
		
		orakuploader_max_exceeded : function() {
			alert("You exceeded the max. limit of 3 images.");
		}
		
	});
});