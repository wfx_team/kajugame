$(document).ready(function(){
	$('#images9ex').orakuploader({
		orakuploader_path : 'orakuploader/',
	
		orakuploader_main_path : 'files',
		orakuploader_thumbnail_path : 'files/tn',
		
		orakuploader_use_sortable : true,
		orakuploader_use_dragndrop : true,
		
		orakuploader_add_image : 'orakuploader/images/add.png',
		orakuploader_add_label : 'Browser for images',
		
		orakuploader_resize_to		 : 600,
		orakuploader_thumbnail_size  : 150,
		orakuploader_maximum_uploads : 100,
		
		//Attaching pre-uploaded images (add only the file names w/o directories)
		orakuploader_attach_images: ['cat.jpg', 'dolphin.jpg', 'lion.jpg'],
	});
});