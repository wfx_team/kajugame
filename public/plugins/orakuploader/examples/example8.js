$(document).ready(function(){
	$('#images8ex').orakuploader({
		orakuploader_path : 'orakuploader/',
	
		orakuploader_main_path : 'files',
		orakuploader_thumbnail_path : 'files/tn',
		
		orakuploader_use_main : true,
		orakuploader_use_sortable : true,
		orakuploader_use_dragndrop : true,
		
		orakuploader_add_image : 'orakuploader/images/add.png',
		orakuploader_add_label : 'Browser for images',
		
		orakuploader_resize_to : 600,
		orakuploader_thumbnail_size : 150,
		orakuploader_maximum_uploads : 100,
		
		orakuploader_finished: function() {
			alert("Uploading finished.");
		},
		orakuploader_main_changed    : function (filename) {
			$("#mainlabel-images").remove();
			$("div").find("[filename='" + filename + "']").append("<div id='mainlabel-images' class='maintext'>Main Image</div>");
			
			alert("Main picture is set to \""+filename+"\".");
		},
		orakuploader_rearranged : function() {
			alert("You rearranged something.");
		},
		orakuploader_picture_deleted : function(filename) {
			alert("Picture \""+filename+ "\" is deleted.");
		}
	});
});