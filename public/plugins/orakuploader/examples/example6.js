$(document).ready(function(){
	$('#images6ex').orakuploader({
		orakuploader_path  		  : '/plugins/orakuploader/examples/orakuploader/',
		// orakuploader_path  		  : '/adpostupload',

		// orakuploader_main_path : '/saleme/tempimages',
		// orakuploader_thumbnail_path : '/saleme/tempimages/tn',

        orakuploader_main_path : '/files',
        orakuploader_thumbnail_path : '/files/tn',
		
		orakuploader_add_image       : '/plugins/orakuploader/images/add.png',
		orakuploader_add_label       : 'Browser for images',
		
		orakuploader_use_sortable : true,
		
		orakuploader_resize_to	     : 600,
		orakuploader_thumbnail_size  : 150
	});
});