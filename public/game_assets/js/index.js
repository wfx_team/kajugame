var score = 0;
var color = "blue";
$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});
function random(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}

function setBG() {
    if (Math.round(Math.random())) {
        return "https://offerhut.lk/offers/2105_img/offerhut.lk_Kokis__sri_lankan_best_offer_322017_01_02_03_29_05972.jpg";
    } else {
        return "https://cdn.pixabay.com/photo/2016/08/18/04/44/bomb-1602109_960_720.png";
    }
}


function dropBox() {
    var length = random(100, ($(".game").width() - 100));
    var velocity = random(850, 10000);
    var size = random(50, 150);
    var thisBox = $("<div/>", {
        class: "box",
        style: "width:" + size + "px; height:" + size + "px; left:" + length + "px; transition: transform " + velocity + "ms linear;"
    });

    //set data and bg based on data
    thisBox.data("test", Math.round(Math.random()));
    if (thisBox.data("test")) {
        thisBox.css({
            "background": "url('https://offerhut.lk/offers/2105_img/offerhut.lk_Kokis__sri_lankan_best_offer_322017_01_02_03_29_05972.jpg')",
            "background-size": "contain"
        });
    } else {
        thisBox.css({
            "background": "url('https://cdn.pixabay.com/photo/2016/08/18/04/44/bomb-1602109_960_720.png')",
            "background-size": "contain"
        });
    }


    //insert gift element
    $(".game").append(thisBox);

    //random start for animation
    setTimeout(function () {
        thisBox.addClass("move");
    }, random(0, 5000));

    //remove this object when animation is over
    thisBox.one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",
        function (event) {
            $(this).remove();
        });
}

for (i = 0; i < 10; i++) {
    dropBox();
}


$(document).on('click', '.box', function () {
    if ($(this).data("test")) {
        score += 1;
        PlaySound('../game_assets/16640_1460665329.mp3');
    } else {
        score -= 1;
        PlaySound('../game_assets/hahaha-Peter_De_Lang-1639076107.mp3');
    }

    $(".score").html(score);
    $(this).remove();
});

var runGame = setInterval(function () {
    for (i = 0; i < 10; i++) {
        dropBox();
    }
}, 5000);

function countdown() {
    var seconds = 5;

    function tick() {
        var counter = document.getElementById("counter");
        seconds--;
        counter.innerHTML = (seconds < 10 ? "0" : "") + String(seconds) + "S";
        if (seconds > 0) {
            setTimeout(tick, 1000);
            // draw();
            // update();
        } else {
            // var yetVisited = localStorage['visited'];
            // if (!yetVisited) {
            //     // open popup
            //     localStorage['visited'] = "yes";
            // }
            var data = {'score': score};
            $.ajax({
                type: "post",
                url: '/game/score/' + score,
                data: data,
                success: function (res) {
                    // $('.deleresons').modal('hide');
                    // $(classname).css('opacity', 0.5);
                    // noty({text: 'Ad deleted Successfully!', layout: 'topRight', type: 'success', timeout: 3000,});
                    // var delay = 5000;
                    // setTimeout(function () {
                    //     window.location = '/my_ads';
                    // }, delay);
                }
            });
            // alert("Game over");
            clearInterval(runGame);
        }
    }

    tick();
}

countdown();

function PlaySound(path) {
    var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', path);
    audioElement.play();
}